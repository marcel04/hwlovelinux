## 11 Nov 2020

La comanda per veure l'informació dels discs durs:

```
root@a34 ~]# fdisk -l
Disk /dev/sda: 465.78 GiB, 500107862016 bytes, 976773168 sectors
Disk model: ST500DM002-1BD14
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0x000c44b9

Device     Boot     Start       End   Sectors   Size Id Type
/dev/sda1            2048 976773119 976771072 465.8G  5 Extended
/dev/sda5  *         4096 209719295 209715200   100G 83 Linux
/dev/sda6       209721344 419436543 209715200   100G 83 Linux
/dev/sda7       419438592 429924351  10485760     5G 82 Linux swap / Solaris

```

Mes específic:

```
[root@a34 ~]# lshw -class disk
  *-disk                    
       description: ATA Disk
       product: ST500DM002-1BD14
       physical id: 0.0.0
       bus info: scsi@0:0.0.0
       logical name: /dev/sda
       version: KC48
       serial: Z6E4HF7Q
       size: 465GiB (500GB)
       capabilities: partitioned partitioned:dos
       configuration: ansiversion=5 logicalsectorsize=512 sectorsize=4096 signature=000c44b9
```

