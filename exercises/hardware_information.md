## Extract hardware info from host with linux
* system



### system model, bios version and bios date, how old is the hardware?

```
[root@a34 ~]# dmidecode -s bios-version
FC
```

mainboard model, link to manual, link to product

```
[root@a34 ~]# dmidecode -s system-product-name
H81M-S2PV
```
###### Manual: [Link](http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#support-manual)
###### Product link: [Link](http://es.gigabyte.com/products/page/mb/ga-h81m-s2pvrev_10#kf)

memory banks (free or occupied)
```
[root@a34 ~]# lshw -class memory
*-bank:0
          description: DIMM DDR3 Synchronous 1400 MHz (0.7 ns)
          product: 9905584-014.A00LF
          vendor: Kingston
          physical id: 0
          serial: 2C380AC0
          slot: ChannelA-DIMM0
          size: 4GiB
          width: 64 bits
          clock: 1400MHz (0.7ns)
     *-bank:1
          description: DIMM [empty]
          product: [Empty]
          vendor: [Empty]
          physical id: 1
          serial: [Empty]
          slot: ChannelA-DIMM1
     *-bank:2
          description: DIMM DDR3 Synchronous 1400 MHz (0.7 ns)
          product: 9905584-014.A00LF
          vendor: Kingston
          physical id: 2
          serial: 2B3877F1
          slot: ChannelB-DIMM0
          size: 4GiB
          width: 64 bits
          clock: 1400MHz (0.7ns)
     *-bank:3
          description: DIMM [empty]
          product: [Empty]
          vendor: [Empty]
          physical id: 3
          serial: [Empty]
          slot: ChannelB-DIMM1
```

how many disks and types can be connected

chipset, link to
Link to an image of the chipset of my motherboard 
* [Link](https://www.gamersnexus.net/images/media/2013/hardware/intel-x79-chipset.jpg)

cpu

cpu model, year, cores, threads, cache
```
[root@a34 ~]# lscpu
Model name:                      Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
Core(s) per socket:              2
Thread(s) per core:              1
    L1d cache:                       64 KiB
L1i cache:                       64 KiB
L2 cache:                        512 KiB
L3 cache:                        3 MiB

socket
```
[root@a34 ~]# ss -s
Total: 1158
TCP:   29 (estab 22, closed 4, orphaned 0, timewait 4)

Transport Total     IP        IPv6
RAW	  1         0         1        
UDP	  18        15        3        
TCP	  25        24        1        
INET	  44        39        5        
FRAG	  0         0         0   
```

pci

```
[root@a34 ~]# lspci
00:00.0 Host bridge: Intel Corporation 4th Gen Core Processor DRAM Controller (rev 06)
00:02.0 VGA compatible controller: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller (rev 06)
00:03.0 Audio device: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller (rev 06)
00:14.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB xHCI (rev 05)
00:16.0 Communication controller: Intel Corporation 8 Series/C220 Series Chipset Family MEI Controller #1 (rev 04)
00:1a.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #2 (rev 05)
00:1b.0 Audio device: Intel Corporation 8 Series/C220 Series Chipset High Definition Audio Controller (rev 05)
00:1c.0 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #1 (rev d5)
00:1c.2 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #3 (rev d5)
00:1c.3 PCI bridge: Intel Corporation 8 Series/C220 Series Chipset Family PCI Express Root Port #4 (rev d5)
00:1d.0 USB controller: Intel Corporation 8 Series/C220 Series Chipset Family USB EHCI #1 (rev 05)
00:1f.0 ISA bridge: Intel Corporation H81 Express LPC Controller (rev 05)
00:1f.2 SATA controller: Intel Corporation 8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode] (rev 05)
00:1f.3 SMBus: Intel Corporation 8 Series/C220 Series Chipset Family SMBus Controller (rev 05)
02:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 06)
03:00.0 PCI bridge: Intel Corporation 82801 PCI Bridge (rev 41)
``` 

number of pci slots, lanes available
- 15 Slots, 0 lanes availables

devices connected

network device, model, kernel module, speed
[root@a34 ~]# lspci -v
00:00.0 Host bridge: Intel Corporation 4th Gen Core Processor DRAM Controller (rev 06)
	Subsystem: Gigabyte Technology Co., Ltd Device 5000
	Flags: bus master, fast devsel, latency 0
	Capabilities: [e0] Vendor Specific Information: Len=0c <?>
	Kernel driver in use: hsw_uncore


audio device, model, kernel module
00:03.0 Audio device: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor HD Audio Controller (rev 06)

vga device, model, kernel module
00:02.0 VGA compatible controller: Intel Corporation Xeon E3-1200 v3/4th Gen Core Processor Integrated Graphics Controller

hard disks
[root@a34 ~]# fdisk -l
Disk /dev/sda: 465.78 GiB, 500107862016 bytes, 976773168 sectors
Disk model: ST500DM002-1BD14
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0x000c44b9

Device     Boot     Start       End   Sectors   Size Id Type
/dev/sda1            2048 976773119 976771072 465.8G  5 Extended
/dev/sda5  *         4096 209719295 209715200   100G 83 Linux
/dev/sda6       209721344 419436543 209715200   100G 83 Linux
/dev/sda7       419438592 429924351  10485760     5G 82 Linux swap / Solaris


/dev/* , model, bus type, bus speed

test fio random (IOPS) and sequential (MBps)
